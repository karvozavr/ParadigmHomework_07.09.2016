# Remove equal adjacent elements
#
# Example input: [1, 2, 2, 3]
# Example output: [1, 2, 3]
def remove_adjacent(lst):
    if len(lst) > 0:
        new_lst = [lst[0]]
    else:
        return lst
    for i in range(1, len(lst)):
        if lst[i] != new_lst[-1]:
            new_lst.append(lst[i])
    return new_lst

# Merge two sorted lists in one sorted list in linear time
#
# Example input: [2, 4, 6], [1, 3, 5]
# Example output: [1, 2, 3, 4, 5, 6]
def linear_merge(lst1, lst2):
    lst = []
    i = 0
    j = 0
    sum_len = len(lst1) + len(lst2)
    while (i + j) < sum_len:
        if i == len(lst1):
            lst += lst2[j:]
            return lst
        if j == len(lst2):
            lst += lst1[i:]
            return lst
        if lst1[i] > lst2[j]:
            lst.append(lst2[j])
            j += 1
        else:
            lst.append(lst1[i])
            i += 1
    return lst
