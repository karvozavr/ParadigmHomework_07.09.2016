# Given a string, if its length is at least 3,
# add 'ing' to its end.
# Unless it already ends in 'ing', in which case
# add 'ly' instead.
# If the string length is less than 3, leave it unchanged.
# Return the resulting string.
#
# Example input: 'read'
# Example output: 'reading'
def verbing(s): 
    if len(s) < 3:
        return s
    if s[-3:] != 'ing':
        s += 'ing'
    else:
        s += 'ly'
    return s


# Given a string, find the first appearance of the
# substring 'not' and 'bad'. If the 'bad' follows
# the 'not', replace the whole 'not'...'bad' substring
# with 'good'.
# Return the resulting string.
#
# Example input: 'This dinner is not that bad!'
# Example output: 'This dinner is good!'
def not_bad(s):
    new_str = ''
    index = s.find('not')
    if index > -1:
        new_str = s[: index]
        index2 = s.find('bad', index)
        if index2 > -1: 
            new_str += 'good'
            new_str += s[index2 + 3 :]
        else:
            return s
    else:
        return s
    return new_str


# Consider dividing a string into two halves.
# If the length is even, the front and back halves are the same length.
# If the length is odd, we'll say that the extra char goes in the front half.
# e.g. 'abcde', the front half is 'abc', the back half 'de'.
#
# Given 2 strings, a and b, return a string of the form
#  a-front + b-front + a-back + b-back
#
# Example input: 'abcd', 'xy'
# Example output: 'abxcdy'
def front_back(a, b):
    new_str = ''
    half_a = int(len(a) / 2 + (len(a) % 2))
    half_b = int(len(b) / 2 + (len(b) % 2))
    new_str += a[: half_a]
    new_str += b[: half_b]
    new_str += a[half_a :]
    new_str += b[half_b :]
    return new_str

if __name__ == '__main__':
    assert(verbing('hello') == 'helloing')
